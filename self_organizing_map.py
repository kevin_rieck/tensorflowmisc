import os
import time

import matplotlib.animation as animation
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from sklearn import cluster
from sklearn.metrics.pairwise import cosine_distances, euclidean_distances, manhattan_distances

from utility_funcs import get_watershed, calc_umatrix, remove_border_label, ColorCarrier


class SOM(object):
    """
    1D or 2D Self-organizing map with sequential learning algorithm with monotonously decreasing learning rate
    and neighbourhood radius
    """

    # To check if the SOM has been trained
    _trained = False

    def __init__(self, m, n, dim, n_iterations=100, alpha=None, sigma=None,
                 wanted_clusters=15, metric='cosine', clustering_method='agg',
                 decay='lin'):
        """
        :param m: number of rows of the map
        :param n: number of columns of the map
        :param dim: number of features
        :param n_iterations: integer number of epochs that are trained
        :param alpha: learning rate
        :param sigma: initial neighbourhood radius
        :param wanted_clusters: integer number of clusters wanted for clustering (only for kmeans and hierarchical clustering)
        :param metric: distance metric used to identify the best-matching unit
        :param clustering_method: string determining the algorithm used for clustering
        """

        # Assign required variables first
        if metric not in ['cosine', 'euclidean', 'manhattan']:
            raise ValueError('Metric must be either cosine, euclidean or manhattan.')
        else:
            self.metric = metric

        # metric function for calculating U-matrix based on the metric for training
        if self.metric == 'cosine':
            self.dist_func = cosine_distances
        elif self.metric == 'manhattan':
            self.dist_func = manhattan_distances
        else:
            self.dist_func = euclidean_distances

        if clustering_method in ['kmeans', 'agg']:
            self.clustering_method = clustering_method
        else:
            raise Exception("Use 'kmeans' for K-Means or 'agg' for agglomerative")

        if alpha is None:
            alpha = 0.3
        else:
            alpha = float(alpha)

        if sigma is None:
            sigma = max(m, n) / 2.0
        else:
            sigma = float(sigma)

        self._m = m
        self._n = n
        self._alpha = alpha
        self.wanted_clusters = wanted_clusters
        self._n_iterations = abs(int(n_iterations))

        self._weightages = None  # list of trained model vectors
        self._locations = None  # list of neuron locations
        self._centroid_grid = None
        self.cluster = None  # int nd array of clusters with shape of som
        self._last_bmus = None
        self.umatrix = None
        self.last_bmu_qe = None  # list of qes of the last input list
        self.state_dependent_qe_dict = {}  # dict of the active states and their mean qe for the last input list
        self.clustered_by_watershed = False  # bool if clustered by watershed
        self.colormap = ColorCarrier().make_cmap('white', 'black')
        # INITIALIZE GRAPH
        self._graph = tf.Graph()

        # POPULATE GRAPH WITH NECESSARY COMPONENTS
        with self._graph.as_default():

            # weight vectors initialised from random distribution
            self._weightage_vects = tf.Variable(tf.random_uniform([m * n, dim], minval=0.0, maxval=1.0, seed=666))

            # location of each neuron as row and column
            self._location_vects = tf.constant(np.array(list(self.neuron_locations(m, n))))

            # PLACEHOLDERS FOR TRAINING INPUTS
            # We need to assign them as attributes to self, since they will be fed in during training

            # The training vector
            self._vect_input = tf.placeholder("float", [dim])
            # Iteration number
            self._iter_input = tf.placeholder("float")

            # CONSTRUCT TRAINING OP PIECE BY PIECE
            # Only the final, 'root' training op needs to be assigned as
            # an attribute to self, since all the rest will be executed
            # automatically during training

            # To compute the Best Matching Unit given a vector
            # Basically calculates the distance between every
            # neuron's weight vector and the input, and returns the
            # index of the neuron which gives the least value

            # Based on the chosen metric the distance function will be assigned to self.distance-op
            if self.metric == 'manhattan':
                distance = tf.reduce_sum(tf.abs(tf.subtract(self._weightage_vects, self._vect_input)), axis=1)
                bmu_index = tf.argmin(distance, 0)
                self.distance = tf.reduce_min(distance)

            elif self.metric == 'cosine':
                input_1 = tf.nn.l2_normalize(self._weightage_vects, 1)
                input_2 = tf.nn.l2_normalize(self._vect_input, 0)
                input_2_2d = tf.expand_dims(input_2, 1)
                cosine_similarity = tf.reduce_sum(tf.matmul(input_1, input_2_2d), axis=1)
                distance = 1.0 - cosine_similarity
                bmu_index = tf.argmax(cosine_similarity, 0)
                self.distance = tf.reduce_min(distance)

            else:
                distance = tf.sqrt(tf.reduce_sum(tf.pow(tf.subtract(self._weightage_vects, self._vect_input), 2), 1))
                bmu_index = tf.argmin(distance, 0)
                self.distance = tf.reduce_min(distance)

            # This will extract the location of the BMU based on the BMU's index
            slice_input = tf.pad(tf.reshape(bmu_index, [1]), np.array([[0, 1]]))
            self.bmu_loc = tf.reshape(tf.slice(self._location_vects, slice_input, tf.cast(tf.constant(np.array([1, 2])),
                                                                                          tf.int64)), [2])

            # calculating other metrics for input based on BMU index for QE evaluation <- independent of self.metric
            self.manhattan_qe = tf.reduce_sum(tf.abs(tf.subtract(self._weightage_vects[bmu_index], self._vect_input)))

            self.euclidean_qe = tf.sqrt(tf.reduce_sum(tf.pow(tf.subtract(self._weightage_vects[bmu_index],
                                                                         self._vect_input), 2)))

            # calculating inputs for cosine distance by normalizing 1D tensors
            cos_input_1 = tf.nn.l2_normalize(self._weightage_vects[bmu_index], 0)
            cos_input_2 = tf.nn.l2_normalize(self._vect_input, 0)
            cosine_similarity = tf.reduce_sum(tf.multiply(cos_input_1, cos_input_2))
            self.cosine_qe = 1.0 - cosine_similarity

            if decay == 'exp':
                # this line will compute the decrease of alpha and sigma as a exponential decay:
                learning_rate_op = tf.exp(tf.negative((6*self._iter_input)/self._n_iterations))
            else:
                # compute alpha and sigma linearly based on the current iteration
                learning_rate_op = tf.subtract(1.0, tf.div(self._iter_input, self._n_iterations))

            _alpha_op = tf.multiply(alpha, learning_rate_op)
            _sigma_op = tf.multiply(sigma, learning_rate_op)

            # Construct the op that will generate a vector with learning rates for all neurons,
            #  based on iteration number and location in comparison to the BMU.
            bmu_distance_squares = tf.reduce_sum(tf.pow(tf.subtract(self._location_vects, self.bmu_loc), 2), 1)
            neighbourhood_func = tf.exp(tf.negative(tf.div(tf.cast(bmu_distance_squares, "float32"),
                                                           tf.multiply(tf.pow(_sigma_op, 2), 2))))
            learning_rate_op = tf.multiply(_alpha_op, neighbourhood_func)

            # Finally, the op that will use learning_rate_op to update the weightage vectors of all neurons based on
            #  a particular input
            learning_rate_multiplier = tf.expand_dims(learning_rate_op, -1)
            weightage_delta = tf.multiply(learning_rate_multiplier,
                                          tf.subtract(self._vect_input, self._weightage_vects))
            new_weightages_op = tf.add(self._weightage_vects, weightage_delta)
            self._training_op = tf.assign(self._weightage_vects, new_weightages_op)

            # INITIALIZE SESSION
            self._sess = tf.Session()  # config=tf.ConfigProto(log_device_placement=True)

            # INITIALIZE VARIABLES
            init_op = tf.global_variables_initializer()
            self._sess.run(init_op)

    @staticmethod
    def neuron_locations(m, n):
        """
        Returns the grid of neuron locations
        :param m: rows of the grid
        :param n: columns of the grid
        :return: array of x, y location on the grid for each neuron
        """

        # Nested iterations over both dimensions
        # to generate all 2-D locations in the map
        for i in range(m):
            for j in range(n):
                yield np.array([i, j])

    def fit(self, input_vects, *args):
        """
        Training of the SOM
        :param input_vects: data that is to be process in shape(n_observation, n_features)
        :param args: ignored, added to enable fitting in loop with other sklearn models
        :return: None, adjusts the model weight vectors saved in self._centroid_grid
        """

        for iter_no in range(self._n_iterations):
            # Train with each vector one by one
            t0 = time.time()
            for input_vect in input_vects:
                self._sess.run(self._training_op,
                               feed_dict={self._vect_input: input_vect,
                                          self._iter_input: iter_no})  # number is necessary for learning rate
            t1 = time.time() - t0
            print('EPOCH: {} --- TIME {:.3f}s'.format(iter_no, t1))

        # Store a centroid grid for easy retrieval later on, closing the Session and performing clustering
        centroid_grid = [[] for _ in range(self._m)]  # same data as self._weightages but better accessibility
        self._weightages = list(self._sess.run(self._weightage_vects))
        self._locations = list(self._sess.run(self._location_vects))
        for i, loc in enumerate(self._locations):
            centroid_grid[loc[0]].append(self._weightages[i])
        self._centroid_grid = centroid_grid
        self._trained = True
        self._calc_clusters(n_cluster=self.wanted_clusters)

    def get_centroids(self):
        """
        Returns a list of 'm' lists, with each inner list containing
        the 'n' corresponding centroid locations as 1-D NumPy arrays.
        """
        if not self._trained:
            raise ValueError("SOM not trained yet")
        return self._centroid_grid

    # TODO ev. remove method use fit_transform instead
    def map_vects(self, input_vects):
        """
        Maps each input vector to the relevant neuron in the SOM
        grid.
        'input_vects' should be an iterable of 1-D NumPy arrays with
        dimensionality as provided during initialization of this SOM.
        Returns a list of 1-D NumPy arrays containing (row, column)
        info for each input vector(in the same order), corresponding
        to mapped neuron.
        """

        if not self._trained:
            raise ValueError("SOM not trained yet")

        to_return = []
        for vect in input_vects:
            min_index = min([i for i in range(len(self._weightages))],
                            key=lambda x: np.linalg.norm(vect -
                                                         self._weightages[x]))
            to_return.append(self._locations[min_index])

        return to_return

    def _get_bmu(self, input_vects):
        """
        Method that determines the location of the BMU for each input vector
        :param input_vects: data that is to be process in shape(n_observation, n_features)
        :return: list of x,y locations of the BMU for each input
        """

        start_time = time.time()

        to_return = []
        quantization_error = []

        # Distance of the BMU is calculated using the distance function that was given to __init__
        # feed_dict requires a number for self._iter_input but it has no effect because train op is not called
        for vect in input_vects:
            loc = self._sess.run(self.bmu_loc, feed_dict={self._vect_input: vect,
                                                          self._iter_input: self._n_iterations})
            qe = self._sess.run(self.distance, feed_dict={self._vect_input: vect,
                                                          self._iter_input: self._n_iterations})
            to_return.append(loc)
            quantization_error.append(qe)

        self.last_bmu_qe = np.array(quantization_error)
        stop_time = time.time() - start_time

        # prints the duration of the calculation and the average QE over all inputs
        print('Dauer = {:.3f} s'.format(stop_time))
        print('QE = {:.3f}'.format(np.mean(np.array(quantization_error))))

        return to_return

    def predict(self, input_vects, save_qes=True):
        """
        Predicts a cluster label for each input in input_vects based on the clusters in self.cluster
        :param input_vects: data that is to be process in shape(n_observation, n_features)
        :return: list of labels, one for each input in input vects
        """
        if not self._trained:
            raise ValueError("SOM not trained yet")

        if self.clustered_by_watershed:
            self._calc_clusters(n_cluster=self.wanted_clusters)
            print('Clustering was by watershed -> changed to {}'.format(self.clustering_method))

        self._last_bmus = self._get_bmu(input_vects)
        label_list = []

        for entry in self._last_bmus:
            label_list.append(self.cluster[entry[0], entry[1]])

        if save_qes:
            self._state_dependent_qe(label_list)

        return label_list

    def predict_w_umatrix(self, input_vects, save_qes=True, numbers_on_plot=True):
        """
        Predicts a label for each input based on the clustering as a result of the watershed transformation
        of the u-matrix, saves figure of clustermap
        :param input_vects: data that is to be process in shape(n_observation, n_features)
        :return: list of labels, one for each input
        """
        if not self.clustered_by_watershed:
            self.cluster = get_watershed(self.get_umatrix())
            self.clustered_by_watershed = True
            clustermap, ax = plt.subplots(1, 1, figsize=(11.69, 11.69/2))
            ax.imshow(self.cluster, cmap=self.colormap)
            n_cluster = np.max(self.cluster)
            ax.set_title('Clusters = {}, Epochs = {}, Metric = {}, lr = {}'.format(n_cluster, self._n_iterations,
                                                                                   self.metric.capitalize(),
                                                                                   self._alpha))
            ax.get_xaxis().set_ticks([])
            ax.get_yaxis().set_ticks([])

            fname = r'{}x{} clusters_{}_epochs_{}_metric_{}_lr_{}_ws.png'.format(self._m, self._n, n_cluster,
                                                                                 self._n_iterations,
                                                                                 self.metric.capitalize(), self._alpha)
            if numbers_on_plot:
                used_numbers = []
                for i in range(self._m):
                    for j in range(self._n):
                        if self.cluster[(i, j)] not in used_numbers:
                            ax.text(j, i, self.cluster[(i, j)], va='top', ha='left', color='w',
                                    bbox={'facecolor': ColorCarrier().faps_colors['green']})
                            used_numbers.append(self.cluster[(i, j)])
                        else:
                            pass
            clustermap.tight_layout()
            self.save_figure(clustermap, fname)

            print('Clustering was by {} -> changed to watershed'.format(self.clustering_method))

        self._last_bmus = self._get_bmu(input_vects)
        label_list = []

        for entry in self._last_bmus:
            label_list.append(self.cluster[entry[0], entry[1]])
        label_list = remove_border_label(label_list)

        if save_qes:
            self._state_dependent_qe(label_list)

        return label_list

    def evaluate_different_qe(self, input_vects, which_qe='manhattan', return_as_nparray=True):
        """
        Calculates QE that is different from the one used during training for an array of input vectors
        :param input_vects: array of input data
        :param which_qe: string denoting the desired QE type, must be manhattan, euclidean or cosine
        :param return_as_nparray: whether to return as array, else list
        :return: either list or array of the wanted QE for each input
        """
        if which_qe not in ['manhattan', 'cosine', 'euclidean']:
            raise ValueError('QE name must be "manhattan", "cosine" or "euclidean"')

        start_time = time.time()

        quantization_error = []

        for vect in input_vects:
            if which_qe == 'manhattan':
                qe = self._sess.run(self.manhattan_qe, feed_dict={self._vect_input: vect,
                                                                  self._iter_input: self._n_iterations})
            elif which_qe == 'cosine':
                qe = self._sess.run(self.cosine_qe, feed_dict={self._vect_input: vect,
                                                               self._iter_input: self._n_iterations})
            else:
                qe = self._sess.run(self.euclidean_qe, feed_dict={self._vect_input: vect,
                                                                  self._iter_input: self._n_iterations})

            quantization_error.append(qe)
        if return_as_nparray:
            quantization_error = np.array(quantization_error)

        stop_time = time.time() - start_time
        print('Dauer = {:.3f} s'.format(stop_time))
        return quantization_error

    # TODO is used for hit histogram and animation -> does not return the states
    def fit_transform(self, input_vects):
        """
        Trains the SOM if untrained and returns the BMU coordinates for given input vectors
        :param input_vects: data that is to be process in shape(n_observation, n_features)
        :return: array with x,y coordinate for each input vector
        """
        if not self._trained:
            self.fit(input_vects)

        bmu_list = self._get_bmu(input_vects)
        return np.array(bmu_list, dtype=np.uint16)

    def _calc_clusters(self, n_cluster, clustering_method=None, numbers_wanted=True):
        """
        Performs clustering based on sklearn's agglomerative or k-means clustering
        Saves plot of the clustering using imshow
        :param n_cluster: number of desired clusters
        :param clustering_method: string determining the clustering algorithm, either 'agg', 'kmeans'
        :param numbers_wanted: bool if image should also show the cluster numbers
        :return: the cluster array, also saved in self.cluster
        """
        grid = np.array(self.get_centroids())
        grid_reshaped = np.reshape(grid, (-1, grid.shape[-1]))

        if clustering_method is None:
            clustering_method = self.clustering_method

        if clustering_method == 'agg':
            clusterer = cluster.AgglomerativeClustering(n_clusters=n_cluster, affinity=self.metric,
                                                        linkage='average')
        elif clustering_method == 'kmeans':
            clusterer = cluster.KMeans(n_clusters=n_cluster, random_state=42, )
        else:
            raise Exception("Use 'kmeans' for K-Means or 'agg' for agglomerative")

        clusterer.fit(grid_reshaped)
        cluster_array = clusterer.labels_.reshape((grid.shape[0], grid.shape[1]))

        clustermap, ax = plt.subplots(1, 1, figsize=(11.69, 11.69/2))
        ax.imshow(cluster_array, cmap=self.colormap)
        ax.set_title('Clusters = {}, Epochs = {}, Metric = {}, lr = {}'.format(n_cluster, self._n_iterations,
                                                                               self.metric.capitalize(),
                                                                               self._alpha))
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])

        fname = r'{}x{} clusters_{}_epochs_{}_metric_{}_lr_{}.png'.format(self._m, self._n, n_cluster,
                                                                          self._n_iterations,
                                                                          self.metric.capitalize(), self._alpha)
        if numbers_wanted:
            used_numbers = []
            for i in range(self._m):
                for j in range(self._n):
                    if cluster_array[(i, j)] not in used_numbers:
                        ax.text(j, i, cluster_array[(i, j)], va='top', ha='left', color='w',
                                bbox={'facecolor': ColorCarrier().faps_colors['green']})
                        used_numbers.append(cluster_array[(i, j)])
                    else:
                        pass

        clustermap.tight_layout()
        self.save_figure(clustermap, fname)
        plt.close(clustermap)

        self.clustered_by_watershed = False
        self.cluster = cluster_array
        return np.copy(cluster_array)

    def get_umatrix(self):
        """
        Calculates the U-matrix representation of the model's weight vectors
        Saves plot of u-matrix
        :return: u-matrix representation as numpy array
        """
        assert self._trained, 'Not trained yet'
        self.umatrix = calc_umatrix(self.get_centroids(), dist_func=self.dist_func)

        umatrix_plot, ax = plt.subplots(1, 1)
        ax.imshow(self.umatrix, cmap=self.colormap, interpolation='none',
                  vmin=np.min(self.umatrix), vmax=np.max(self.umatrix))
        ax.set_title('{}x{}, Epochs = {}, Metric = {}, lr = {}'.format(self._m, self._n, self._n_iterations,
                                                                       self.metric.capitalize(), self._alpha))
        fname = r'{}x{} som_{}_epochs_{}_metric_{}_lr_{}.png'.format(self._m, self._n, self.__class__.__name__,
                                                                     self._n_iterations, self.metric.capitalize(),
                                                                     self._alpha)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])

        umatrix_plot.tight_layout()
        self.save_figure(umatrix_plot, fname)
        plt.close(umatrix_plot)

        return self.umatrix

    def hit_histo(self):
        """
        Plots a hit histogram based on the BMUs of the last predictions
        Growing circles represent number of hits and the nackground is the clustermap
        :return: None, plots hit histogram
        """
        assert self._last_bmus is not None
        y = [point[0] for point in self._last_bmus]
        x = [point[1] for point in self._last_bmus]
        size_array = np.zeros(self.cluster.shape)
        for _x, _y in zip(x, y):
            size_array[_y][_x] += 1
        s = size_array[y, x]
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cluster_map = ax.imshow(self.cluster, cmap=self.colormap)
        scatter = ax.scatter(x, y, s=s, edgecolors='w', facecolors='k', alpha=0.5)

        fname = r'histo_{}x{} som_{}_epochs_{}_metric_{}_lr_{}.png'.format(self._m, self._n, self.__class__.__name__,
                                                                           self._n_iterations, self.metric.capitalize(),
                                                                           self._alpha)
        self.save_figure(fig, fname)
        plt.show(fig)

    def override_clusters(self):
        """
        Overrides a chosen cluster with a new cluster number both based on user input
        :return: None, alters self.cluster
        """
        plt.figure()
        plt.imshow(self.cluster)
        plt.show()

        # User Input für die Clusteränderung
        old_cluster_number = int(input("Alte Clusternummer eingeben (int):"))
        new_cluster_number = int(input("Neue Clusternummer eingeben (int):"))

        possible_values = np.unique(self.cluster)
        if new_cluster_number not in possible_values or old_cluster_number not in possible_values:
            raise ValueError('Clusternummer nicht zulässig, nur Werte von {} bis {}'.format(self.cluster.min(),
                                                                                            self.cluster.max()))

        mask_array = self.cluster == old_cluster_number
        self.cluster[mask_array] = new_cluster_number

        # Neusortierung der geänderten Cluster aufsteigend nach Clusternummer
        incomplete_order = sorted([i for i in np.unique(self.cluster)])
        for counter, value in enumerate(incomplete_order):
            self.cluster[(self.cluster == value)] = counter

        plt.imshow(self.cluster)
        plt.show()

    def animate_bmu_trajectory(self, tail=5):
        """
        Creates animation of the BMU trajectory
        :param tail: number of values that are shown at the same time
        :return: None, creates animation
        """
        assert self._last_bmus is not None
        plt.close()
        fig = plt.figure()
        ax = fig.add_subplot(111)
        surface = ax.imshow(self.get_umatrix(), cmap=self.colormap, interpolation='bilinear')
        hits, = ax.plot([], [], marker='o', ls='dashed', markerfacecolor='r', markeredgecolor='none',
                        color='y', markersize=4.0)
        fig.suptitle('BMU trajectory')
        data = np.array(self._last_bmus)

        def update_trajectory(num, data, hits, tail):
            if num > tail:
                hits.set_data(data[num-tail:num, 1], data[num-tail:num, 0])
            else:
                hits.set_data(data[:num, 1], data[:num, 0])
            return hits,

        trajectory_ani = animation.FuncAnimation(fig, update_trajectory, frames=len(data),
                                                 fargs=(data, hits, tail), interval=100)

        plt.show()

    def _state_dependent_qe(self, label_list):
        """
        Function that calculates the quantization error specific to each cluster
        The average QE for each state and each sample (1 pump) is saved in a dictionary
        :param label_list: List of the labels of predicted by the SOM
        :return: None, saves in dictionary self.state_dependent_qe_dict
        """
        label_set = set(label_list)
        label_array = np.array(label_list, dtype=np.float32)  # float because qe_array also float

        if len(label_array.shape) < 2:
            label_array = np.reshape(label_array, newshape=(-1, 1))
            print('Label array shape changed to {}'.format(label_array.shape))
        if len(self.last_bmu_qe.shape) < 2:
            self.last_bmu_qe = np.reshape(self.last_bmu_qe, newshape=(-1, 1))
            print('Last QE array shape changed to {}'.format(self.last_bmu_qe.shape))

        combined_array = np.hstack((label_array, self.last_bmu_qe))

        for value in label_set:
            debug = combined_array[np.where(combined_array[:, 0] == value)]
            average_qe = np.mean(debug[:, 1])
            try:
                self.state_dependent_qe_dict[value].append(average_qe)
            except KeyError:
                self.state_dependent_qe_dict[value] = [average_qe]

    def plot_state_dependent_qe(self):
        n_plots = len(self.state_dependent_qe_dict.keys())
        n_rows = 4
        n_cols = n_plots//4
        if n_plots % 4:
            n_cols += 1

        qe_fig, ax = plt.subplots(n_rows, n_cols, figsize=(11.69, 11.69))
        axes = ax.ravel()
        for count, key in enumerate(sorted(self.state_dependent_qe_dict.keys())):
            axes[count].plot(self.state_dependent_qe_dict[key], c='k', marker='o', label='State {}'.format(int(key)))
            axes[count].legend(loc='upper right', fontsize='small')
        qe_fig.tight_layout()
        plt.show(qe_fig)

    def plot_cluster_mean_spectrum(self, cluster_number, input_vector=None):
        """
        plot function for comapring input and mean freq spectrum of corresponding bmu
        :param cluster_number: show mean spectrum of this cluster number
        :param input_vector: calc best cluster and calc bmu and plots mean spectrum (ignores cluster_number)
        :return: figure, axes
        """
        plt.close('all')
        centroid_grid = np.array(self.get_centroids())
        grid_shape = centroid_grid.shape
        lines = []
        labels = []
        color_lst = []
        if input_vector is not None:
            loc = self._get_bmu([input_vector])
            bmu_spectrum = centroid_grid[loc[0][0], loc[0][1]]
            lines.append(bmu_spectrum)
            labels.append('Spectrum of BMU')
            color_lst.append(ColorCarrier().faps_colors['green'])
            lines.append(input_vector)
            labels.append('Input spectrum')
            color_lst.append(ColorCarrier().faps_colors['red'])
            cluster_number = self.cluster[loc[0][0], loc[0][1]]

        centroid_grid = np.reshape(centroid_grid, (grid_shape[0]*grid_shape[1], grid_shape[-1]))
        cluster_array = self.cluster.reshape(grid_shape[0]*grid_shape[1], )
        idx = np.where(cluster_array == cluster_number)
        subset = centroid_grid[idx]
        print(subset.shape)

        spectrum = np.mean(subset, axis=0)
        lines.append(spectrum)
        labels.append('Mean spectrum of cluster {}'.format(int(cluster_number)))
        color_lst.append(ColorCarrier().faps_colors['black'])
        x_axis_ticks = np.linspace(0, len(spectrum)-1, 6)

        spectrum_fig, axes = plt.subplots(1, 1, figsize=(8, 4))
        for line, label, col in zip(lines, labels, color_lst):
            axes.plot(line, c=col, label=label)
        axes.set_xticks(ticks=x_axis_ticks)
        axes.set_xticklabels(labels=[0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
        axes.set_xlabel(r'Rel. Frequency $\frac{F}{F_{max}}$', size=16)
        axes.set_ylabel('PSD [dB]', size=16)
        axes.tick_params(axis='both', labelsize=14)
        axes.legend(fontsize=14)
        axes.grid()
        spectrum_fig.tight_layout()
        plt.show(spectrum_fig)
        return spectrum_fig, axes

    def create_mapping_video(self, input_vectors, file_title,
                             path_to_ffmpeg=r'C:\Users\Apex\ffmpeg\bin\ffmpeg.exe',
                             save_ani=False):
        """
        creates animation showing which input vector has been mapped where and to which cluster
        :param file_title: string with title if save_ani, without '.mp4' file extension
        :param input_vectors: vectors for which to generate the animation
        :param path_to_ffmpeg: full path to ffmpeg.exe, has to be installed
        :param save_ani: bool to save the animation as a mp4 vid in same directory
        :return: None
        """
        if not os.path.exists(r'.\videos'):
            os.mkdir(r'.\videos')

        if self.umatrix is None:
            self.umatrix = self.get_umatrix()

        plt.rcParams['animation.ffmpeg_path'] = path_to_ffmpeg
        writer = animation.FFMpegWriter()
        bmu_coordinates = self._get_bmu(input_vectors)
        fig = plt.figure(figsize=(20, 10))
        plt.subplot(2, 2, 1)
        plt.title('U-Matrix')
        plt.imshow(self.umatrix, cmap=self.colormap)

        ims = []
        for i in range(len(bmu_coordinates)):
            x = bmu_coordinates[i][0]
            y = bmu_coordinates[i][1]
            im = plt.scatter(y, x, c='black', edgecolors='white')
            ims.append([im])

        ani = animation.ArtistAnimation(fig, ims, interval=300, blit=True, repeat_delay=1000)
        plt.subplot(2, 2, 2)
        plt.title('Clusters')
        plt.imshow(self.cluster, cmap=plt.cm.get_cmap('gnuplot', len(np.unique(self.cluster))))
        ims3 = []
        for i in range(len(bmu_coordinates)):
            x = bmu_coordinates[i][0]
            y = bmu_coordinates[i][1]
            im = plt.scatter(y, x, c='black', edgecolors='white')
            ims3.append([im])

        ani3 = animation.ArtistAnimation(fig, ims3, interval=300, blit=True, repeat_delay=1000)

        plt.subplot(2, 1, 2)
        plt.title('Frequenzbänder aus dem Spectrogram')
        ims2 = []
        plt.plot(input_vectors)
        for j in range(len(input_vectors)):
            im2 = plt.axvline(x=j, color='black')
            ims2.append([im2])

        ani2 = animation.ArtistAnimation(fig, ims2, interval=300, blit=True, repeat_delay=1000)
        if save_ani:
            ani.save(r'.\videos\{}.mp4'.format(file_title), extra_anim=[ani2, ani3], writer=writer)

        plt.show()

    @staticmethod
    def save_figure(fig_id, filename, foldername='images'):
        working_dir = os.getcwd()
        folder_path = os.path.join(working_dir, foldername)
        if not os.path.exists(folder_path):
            os.mkdir(folder_path)
        path = os.path.join(folder_path, filename)
        fig_id.savefig(path, bbox_inches='tight', format='png', dpi=600)
        print('Saving figure in {}'.format(str(os.path.abspath(folder_path))))
